package com.ostapiuk;

import com.ostapiuk.model.Plateau;

public class Main{
    public static void main(String[] args) {
        Plateau plateau = new Plateau();
        System.out.println(plateau.getLength(new int[]{1, 2, 3,
                4, 4, 4, 5, 5, 6, 6, 6, 6}));
        System.out.println(plateau.getLocation(new int[]{1, 2, 3,
                4, 4, 4, 5, 5, 6, 6, 6, 6}));

        System.out.println(plateau.getLength(new int[]{1, 2, 3, 4, 4, 4, 5, 5, 6}));
        System.out.println(plateau.getLocation(new int[]{1, 2, 3, 4, 4, 4, 5, 5, 6}));

        System.out.println(plateau.getLength(new int[]{1, 1, 1, 1, 2, 3, 4, 4, 4, 5, 5, 6}));
        System.out.println(plateau.getLocation(new int[]{1, 1, 1, 1, 2, 3, 4, 4, 4, 5, 5, 6}));
    }
}
