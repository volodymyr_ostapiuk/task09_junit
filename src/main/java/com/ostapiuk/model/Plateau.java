package com.ostapiuk.model;

/*
 * Given an array of integers, find the length and location of the
 * longest contiguous sequence of equal values where the values of
 * the elements just before and just after this sequence are smaller.
 */
public class Plateau {
    public int getLength(int[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException();
        }
        int length = 1;
        for (int i = 1; i < data.length; i++) {
            if (data[i - length] == data[i]) {
                length++;
            }
        }
        return length;
    }

    public int getLocation(int[] data) {
        if (data.length == 0) {
            throw new IllegalArgumentException();
        }
        int count = 1;
        int length = 1;
        int location = 0;
        for (int i = 1; i < data.length; i++) {
            if (data[i - 1] == data[i]) {
                count++;
                if(count > length) {
                    length = Math.max(length, count);
                    location = i - length + 2;
                }
            } else {
                count = 1;
            }
        }
        return location;
    }
}
