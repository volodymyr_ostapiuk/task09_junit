package com.ostapiuk.view;

import com.ostapiuk.model.Plateau;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private Plateau plateau;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public void myMenu() {
        plateau = new Plateau();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Get length of plateau");
        menu.put("2", "2 - Get location of plateau");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", plateau::getLength);
        menuMethods.put("2", plateau::getLocation);
        showMenu();
    }

    private void outputMenu() {
        for (String command : menu.values()) {
            System.out.println(command);
        }
    }

    private void showMenu() {
        Scanner scannerKey = new Scanner(System.in);
        String key;
        for ( ; ; ) {
            outputMenu();
            System.out.print("Select menu point: ");
            key = scannerKey.nextLine().toUpperCase();
            if (key.equals("Q")) {
                break;
            }
            try {
                menuMethods.get(key).print(new int[]{1, 1, 1, 1, 2, 3, 4, 4, 4, 5, 5, 6});
            } catch (ArithmeticException e) {
                e.getStackTrace();
            }
        }
    }
}
