package com.ostapiuk.view;

@FunctionalInterface
public interface Printable {
  void print(int[] data);
}
