package com.ostapiuk.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlateauTest {

    @Test
    public void testGetLength() {
        Plateau plateau = new Plateau();
        assertEquals(plateau.getLength(new int[]{1, 2, 3,
                4, 4, 4, 5, 5, 6, 6, 6, 6}), 4);
        System.out.println("getLength() is correct!");
    }

    @Test
    public void testGetLocation() {
        Plateau plateau = new Plateau();
        assertEquals(plateau.getLocation(new int[]{1, 1, 1, 1,
                2, 3, 4, 4, 4, 5, 5, 6}), 1);
        System.out.println("getLocation() is correct!");
    }
}
