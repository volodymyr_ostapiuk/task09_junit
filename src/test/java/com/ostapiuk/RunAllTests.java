package com.ostapiuk;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeClassNamePatterns;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("com.ostapiuk")
@IncludeClassNamePatterns({"^.*$"})
public class RunAllTests {

}
